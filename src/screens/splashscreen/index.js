import React, { Component } from "react";
import { Image } from "react-native";
import PointziReact from 'pointzi-react';

const splashscreen = require("../../../assets/splashscreen.png");

export default class SplashPage extends Component {
  componentWillMount(){
      PointziReact.pointziReactWillDidMount('SplashScreenView');
  }
  render() {
    // eslint-disable-line class-methods-use-this
    return (
      <Image
        source={splashscreen}
        style={{ flex: 1, height: null, width: null }}
      />
    );
  }
}
