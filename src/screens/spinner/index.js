import React, { Component } from "react";
import PointziReact from 'pointzi-react';

import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Spinner,
  Left,
  Right,
  Body
} from "native-base";

import styles from "./styles";

class NHSpinner extends Component {
  // eslint-disable-line
  componentWillMount() {
      PointziReact.pointziReactWillDidMount('SpinnerView');
  }
  render() {
    return (
      <Container style={styles.container} nativeID="Spinner View">
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Spinner</Title>
          </Body>
          <Right />

        </Header>

        <Content>
          <Spinner />
          <Spinner color="red" />
          <Spinner color="green" />
          <Spinner color="blue" />
        </Content>
      </Container>
    );
  }
}

export default NHSpinner;
