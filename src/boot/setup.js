import React, { Component } from "react";
import { StyleProvider } from "native-base";

import App from "../App";
import getTheme from "../theme/components";
import variables from "../theme/variables/commonColor";
import {NativeModules} from 'react-native';

export default class Setup extends Component {
  constructor(props) {
	super(props);
	var PointziReact = NativeModules.PointziReact;
    var cuid = 'react-pz-'+Date.now();
    PointziReact.tagCuid(cuid);
    PointziReact.tagString('sh_first_name','demo-user-name');
    PointziReact.tagNumeric('qa_number',12);
    PointziReact.tagDatetime('qa_datetime',"2018-05-15T16:12:22.123Z");
  }
  render() {
    return (
      <StyleProvider style={getTheme(variables)}>
        <App />
      </StyleProvider>
    );
  }
}
